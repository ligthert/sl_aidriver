class CfgPatches
{
	class SL_AIDriver
	{
		units[] = {};
		weapons[] = {};
		requiredAddons[] = {"cba_events"};
		author[]= {"Sacha Ligthert"};
	};
};

class Extended_Init_EventHandlers
{
  class LandVehicle
  {
    init = "if (local (_this select 0)) then { _this execVM '\addEH\init.sqf'; };";
  };
};
