_obj = _this select 0;

_obj addEventHandler ["HandleDamage", {
  _damage = _this select 2;
  _source = _this select 3;
  player sideChat format["%1",_this];
  diag_log format["%1",_this];
  if ( isNull _source ) then { _damage = 0; };
  _damage;
  }]
